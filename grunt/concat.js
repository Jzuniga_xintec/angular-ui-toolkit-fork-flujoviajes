//------------------------------------------------------
// Company: Peanut Hub
// Author: dmunozgaete@gmail.com
// 
// Description: Unify all into one 'big' file
// 
// URL: https://www.npmjs.com/package/grunt-contrib-concat
// 
/// NOTE: If you want to add dependdencies THIS IS THE FILE ;)!
//------------------------------------------------------
module.exports = function(grunt, options)
{

    var bower = grunt.file.readJSON('bower.json');
    var banner = grunt.template.process(grunt.file.read('banner.txt'),
    {
        data:
        {
            authors: bower.authors[0],
            description: bower.description,
            homepage: bower.homepage,
            version: bower.version
        }
    });

    var conf = {
        production_js:
        {
            options:
            {
                separator: ';',
                banner: banner
            },
            files:
            {
                'dist/angular-uk-toolkit.js': [
                    'src/html_templates.js',
                    'src/uk-toolkit.js',
                    'src/js/**/*.js'
                ]
            }
        },

        production_css: {
            options: {
                separator: '\n',
                banner: banner
            },
            files: {
                 'dist/angular-uk-toolkit.css': [
                    'src/**/*.css'
                ]
            }
        }
    };
    //---------------------------------------------------------------
    return conf;
};
